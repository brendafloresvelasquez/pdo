<?php



$host    = "localhost";

$usuario  = "root";

$contraseña  = "";

$base_datos   = "database";

$dsn     = "mysql:host=$host;dbname=$base_datos";



try{

  $conexion = new PDO($dsn, $usuario, $contraseña);

  $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  echo "Se ha establecido una conexión con el servidor de bases de datos.";

}

catch(PDOException $e){

  echo "Error en la conexion a la base de datos: ".$e->getMessage();

}



$conexion = null;



?>